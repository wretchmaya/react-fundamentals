import { render, screen } from '@testing-library/react';
import App from './App';
import { createBrowserHistory } from 'history';
import { Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';

const userInitialState = {
	isAuth: false,
	name: '',
	email: '',
	token: '',
};

const mockStore = configureStore([]);
const store = mockStore(userInitialState);

test('renders header logo', () => {
	render(
		<Provider store={store}>
			<Router history={createBrowserHistory}>
				<App />
			</Router>
		</Provider>
	);
	const headerLogo = screen.getByText(/React Fundamentals #5/i);
	expect(headerLogo).toBeInTheDocument();
});

// npm install redux-mock-store --save-dev
