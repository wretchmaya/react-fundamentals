import {
	addNewAuthorRequest,
	getAuthorsRequest,
} from '../../api/authors/authors';
import { isResponseOk } from '../../helpers/isResponseOk';
import { addNewAuthorAction, getAuthorsAction } from './actionCreators';

export const thunkGetAuthorsRequest = () => async (dispatch) => {
	try {
		const response = await getAuthorsRequest();
		if (isResponseOk(response)) {
			dispatch(getAuthorsAction(response.data.result));
		}
	} catch (error) {
		throw error;
	}
};

export const thunkPostNewAuthor =
	({ name, token }) =>
	async (dispatch) => {
		try {
			const response = await addNewAuthorRequest({ name }, token);
			if (isResponseOk(response)) {
				dispatch(addNewAuthorAction(response.data.result));
			}
		} catch (error) {
			throw error;
		}
	};
