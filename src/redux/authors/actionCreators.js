import { AUTHORS } from './actionType';

export const addNewAuthorAction = (payload) => ({
	type: AUTHORS.ADD_NEW_AUTHOR,
	payload,
});

export const getAuthorsAction = (payload) => ({
	type: AUTHORS.FETCH_AUTHORS,
	payload,
});
