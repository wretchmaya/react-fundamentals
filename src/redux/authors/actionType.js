export const AUTHORS = {
	ADD_NEW_AUTHOR: 'add new author',
	FETCH_AUTHORS: 'fetch authors',
};
