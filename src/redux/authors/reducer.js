import { AUTHORS } from './actionType';
const authorsInitialState = {
	authors: [],
};

export default function authorsReducer(state = authorsInitialState, action) {
	switch (action.type) {
		case AUTHORS.ADD_NEW_AUTHOR:
			return {
				...state,
				authors: [...state.authors, action.payload],
			};
		case AUTHORS.FETCH_AUTHORS:
			return {
				...state,
				authors: [...action.payload],
			};
		default:
			return state;
	}
}
