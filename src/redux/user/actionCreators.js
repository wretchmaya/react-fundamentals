import { USER } from './actionType';

export const logInAction = (payload) => ({
	type: USER.LOG_IN,
	payload,
});

export const logOutAction = () => ({
	type: USER.LOG_OUT,
});

export const getProfileInfoAction = (payload) => ({
	type: USER.GET_PROFILE_INFO,
	payload,
});

export const successRegisterAction = () => ({
	type: USER.SUCCESSFUL_REGISTRATION,
});

export const resetStateAction = () => ({
	type: USER.RESET_STATE,
});
