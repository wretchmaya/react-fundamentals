import { USER } from './actionType';
const userInitialState = {
	isAuth: false,
	name: '',
	email: '',
	token: '',
	role: '',
};

export default function userReducer(state = userInitialState, action) {
	switch (action.type) {
		case USER.LOG_IN:
			return {
				...state,
				name: action.payload.user.name,
				email: action.payload.user.email,
				isAuth: true,
				token: action.payload.token,
			};
		case USER.LOG_OUT:
			return {
				...state,
				name: '',
				email: '',
				isAuth: false,
				token: '',
			};
		case USER.GET_PROFILE_INFO:
			return {
				...state,
				role: action.payload,
			};
		case USER.SUCCESSFUL_REGISTRATION:
			return {
				...state,
				isRegisteredSuccessfully: true,
			};
		case USER.RESET_STATE: {
			return {
				...state,
				isRegisteredSuccessfully: false,
			};
		}
		default:
			return state;
	}
}
