export const USER = {
	LOG_IN: 'login',
	LOG_OUT: 'logout',
	GET_PROFILE_INFO: 'get profile info',
	SUCCESSFUL_REGISTRATION: 'successful registration',
	RESET_STATE: 'reset state',
};
