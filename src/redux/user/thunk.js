import {
	getProfileInfoAction,
	logInAction,
	logOutAction,
	successRegisterAction,
} from './actionCreators';
import {
	getProfileInfoRequest,
	logInRequest,
	logOutRequest,
	registerRequest,
} from '../../api/user/user';
import { isResponseOk } from '../../helpers/isResponseOk';

export const thunkLogInRequest = (userCredentials) => async (dispatch) => {
	try {
		const response = await logInRequest(userCredentials);
		const { result: token, user } = response.data;
		if (isResponseOk(response)) {
			dispatch(logInAction({ token, user }));
		}
	} catch (error) {
		throw error;
	}
};

export const thunkLogOutRequest = (token) => async (dispatch) => {
	try {
		const response = await logOutRequest(token);
		if (isResponseOk(response)) {
			dispatch(logOutAction());
		}
	} catch (error) {
		throw error;
	}
};

export const thunkGetProfileInfo = (token) => async (dispatch) => {
	try {
		const response = await getProfileInfoRequest(token);
		if (isResponseOk(response)) {
			dispatch(getProfileInfoAction(response.data.result.role));
		}
	} catch (error) {
		throw error;
	}
};

export const thunkRegisterRequest = (userCredentials) => async (dispatch) => {
	try {
		const response = await registerRequest(userCredentials);
		if (isResponseOk(response)) {
			dispatch(successRegisterAction());
		}
	} catch (error) {
		throw error;
	}
};
