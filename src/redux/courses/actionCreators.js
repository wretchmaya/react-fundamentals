import { COURSES } from './actionType';

export const getCoursesAction = (payload) => ({
	type: COURSES.FETCH_COURSES,
	payload,
});

export const deleteCourseAction = (payload) => ({
	type: COURSES.DELETE_COURSE,
	payload,
});

export const searchCourseAction = (payload) => ({
	type: COURSES.SEARCH_COURSE,
	payload,
});

export const addNewCourseAction = (payload) => {
	return {
		type: COURSES.ADD_NEW_COURSE,
		payload,
	};
};

export const getCurrentCourseForUpdate = (payload) => ({
	type: COURSES.GET_CURRNT_COURSE,
	payload,
});

export const updateCurrentCourseAction = (payload) => ({
	type: COURSES.UPDATE_COURSE,
	payload,
});

export const resetFormStateAction = () => ({
	type: COURSES.RESET_STATE,
});

export const closeCourseFormAction = () => ({
	type: COURSES.CLOSE_COURSE_FORM,
});

export const openCourseForm = () => ({
	type: COURSES.OPEN_COURSE_FORM,
});
