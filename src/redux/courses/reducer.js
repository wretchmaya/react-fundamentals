import { COURSES } from './actionType';
const coursesInitialState = {
	courses: [],
};

export default function coursesReducer(state = coursesInitialState, action) {
	switch (action.type) {
		case COURSES.ADD_NEW_COURSE:
			return {
				...state,
				courses: [...state.courses, action.payload],
			};
		case COURSES.FETCH_COURSES:
			return {
				...state,
				courses: [...action.payload],
			};
		case COURSES.DELETE_COURSE:
			return {
				...state,
				courses: [
					...state.courses.filter((course) => course.id !== action.payload.id),
				],
			};
		case COURSES.SEARCH_COURSE:
			return {
				...state,
				courses: [...action.payload],
			};
		case COURSES.GET_CURRNT_COURSE:
			return {
				...state,
				currentCourse: {
					...state.courses.reduce(
						(curr, prev) =>
							prev.id === action.payload ? (curr = { ...prev }) : curr,
						{}
					),
				},
			};
		case COURSES.UPDATE_COURSE:
			return {
				...state,
				courses: [
					...state.courses.map((c) =>
						c.id === action.payload.id ? (c = { ...action.payload }) : c
					),
				],
			};
		case COURSES.RESET_STATE: {
			return {
				...state,
				courses: [...state.courses],
				currentCourse: {},
			};
		}
		default:
			return state;
	}
}
