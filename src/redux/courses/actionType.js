export const COURSES = {
	ADD_NEW_COURSE: 'add new course',
	DELETE_COURSE: 'delete course',
	UPDATE_COURSE: 'update course',
	FETCH_COURSES: 'fetch courses',
	SEARCH_COURSE: 'search course',
	GET_CURRNT_COURSE: 'get current course',
	RESET_STATE: 'reset state',
	CLOSE_COURSE_FORM: 'close course form',
	OPEN_COURSE_FORM: 'open course form',
};
