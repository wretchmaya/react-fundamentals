import coursesReducer from './reducer';
import { COURSES } from './actionType';

describe('courses reducer', () => {
	let inititalState;
	beforeEach(() => {
		inititalState = {
			courses: [],
		};
	});

	test('should return initial state', () => {
		const newState = coursesReducer(inititalState, { type: 'TEST' });
		expect(newState).toEqual(inititalState);
	});

	test('should handle ADD_COURSE', () => {
		const payload = {
			data: 'test',
		};
		const newState = coursesReducer(inititalState, {
			type: COURSES.ADD_NEW_COURSE,
			payload,
		});

		expect(newState).toEqual({
			...inititalState,
			courses: [...inititalState.courses, payload],
		});
	});

	test('should handle GET_COURSES', () => {
		const payload = [{ test: 'test' }, { test2: 'test2' }];
		const newState = coursesReducer(inititalState, {
			type: COURSES.FETCH_COURSES,
			payload,
		});

		expect(newState).toEqual({
			...inititalState,
			courses: [...payload],
		});
	});
});
