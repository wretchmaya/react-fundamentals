import {
	deleteCourseByIdRequest,
	getCourseByIdRequest,
	getCoursesRequest,
	postCourseRequest,
	updateCourseByIdRequest,
} from '../../api/courses/courses';
import {
	addNewCourseAction,
	deleteCourseAction,
	getCoursesAction,
	getCurrentCourseForUpdate,
	updateCurrentCourseAction,
} from './actionCreators';
import { isResponseOk } from '../../helpers/isResponseOk';

export const thunkGetCourseByIdRequest = (id) => async (dispatch) => {
	try {
		const response = await getCourseByIdRequest(id);
		if (isResponseOk(response)) {
			dispatch(getCurrentCourseForUpdate(response.data.result));
		}
	} catch (error) {
		throw error;
	}
};

export const thunkGetCoursesRequest = () => async (dispatch) => {
	console.log('getting courses');
	try {
		const response = await getCoursesRequest();
		if (isResponseOk(response)) {
			dispatch(getCoursesAction(response.data.result));
		}
	} catch (error) {
		throw error;
	}
};

export const thunkDeleteCourseByIdRequest =
	({ id, token }) =>
	async (dispatch) => {
		try {
			const response = await deleteCourseByIdRequest(id, token);
			if (isResponseOk(response)) {
				dispatch(deleteCourseAction({ id }));
			}
		} catch (error) {
			throw error;
		}
	};

export const thunkPostCourseRequest =
	({ courseData, token }) =>
	async (dispatch) => {
		try {
			const response = await postCourseRequest(courseData, token);
			if (isResponseOk(response)) {
				dispatch(addNewCourseAction(response.data.result));
			}
		} catch (error) {
			throw error;
		}
	};

export const thunkUpdateCourseByIdRequest =
	({ courseData, token }) =>
	async (dispatch) => {
		try {
			const response = await updateCourseByIdRequest(
				courseData,
				courseData.id,
				token
			);
			if (isResponseOk(response)) {
				dispatch(updateCurrentCourseAction(response.data.result));
			}
		} catch (error) {}
	};
