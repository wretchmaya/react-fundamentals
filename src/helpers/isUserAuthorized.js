export const isUserAuthorized = () => {
	return localStorage.getItem('token');
};
