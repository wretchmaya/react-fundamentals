import { minutesToHours } from './minutesToHours';

test('converts 120 minutes to 02:00 hours', () => {
	expect(minutesToHours(120)).toBe('02:00');
});

test('converts 600 minutes to 10:00 hours', () => {
	expect(minutesToHours(600)).toBe('10:00');
});
