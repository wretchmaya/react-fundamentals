import React from 'react';
import Header from './components/Header/index.js';
import Courses from './components/Courses/index.js';
import CourseForm from './components/CourseForm/index.js';
import { Switch, Route } from 'react-router-dom';
import Login from './components/Login/index.js';
import Registration from './components/Registration/index.js';
import CourseInfo from './components/CourseInfo/CourseInfo';
import PrivateRoute from './components/PrivateRoute/PrivateRoute';
import { withRouter } from 'react-router';
import { useSelector } from 'react-redux';

const App = (props) => {
	const isLoggedIn = useSelector((state) => state.user.isAuth);
	const userRole = useSelector((state) => state.user.role);
	const isAdmin = userRole === 'admin';

	const addNewCourse = () => {
		props.history.push('/courses/add');
	};

	const closeCourseForm = () => {
		setTimeout(() => {
			props.history.push('/courses');
		}, 600);
	};

	return (
		<>
			<Header />
			<Switch>
				<Route exact path='/'>
					<Login />
				</Route>
				<Route path='/login'>
					<Login />
				</Route>
				<Route path='/registration'>
					<Registration />
				</Route>
				<PrivateRoute
					exact
					path={'/courses'}
					redirectTo={'/login'}
					isAllowed={isLoggedIn}
				>
					<Courses addNewCourse={addNewCourse} />
				</PrivateRoute>
				<PrivateRoute
					exact
					path='/courses/add'
					redirectTo={'/login'}
					isAllowed={isAdmin && isLoggedIn}
					children={<CourseForm closeCourseForm={closeCourseForm} />}
				></PrivateRoute>
				<PrivateRoute
					exact
					path={'/courses/:id'}
					children={<CourseInfo />}
					redirectTo={'/login'}
					isAllowed={isLoggedIn}
				></PrivateRoute>
				<PrivateRoute
					path={'/courses/update/:id'}
					children={<CourseForm closeCourseForm={closeCourseForm} />}
					redirectTo={'/courses'}
					isAllowed={isAdmin && isLoggedIn}
				></PrivateRoute>
			</Switch>
		</>
	);
};

export default withRouter(App);
