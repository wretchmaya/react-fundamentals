import React from 'react';
import styles from './style.module.scss';
import classNames from 'classnames/bind';
import { types } from './types';

const cx = classNames.bind(styles);

const Button = ({ text, onClick, customClass }) => {
	return (
		<button className={cx('button', customClass)} onClick={onClick}>
			{text}
		</button>
	);
};

Button.propTypes = types;

export default Button;
