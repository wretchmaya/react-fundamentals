import PropTypes from 'prop-types';

export const types = {
	text: PropTypes.string.isRequired,
	onClick: PropTypes.func,
	customClass: PropTypes.string,
};
