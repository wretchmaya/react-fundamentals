import PropTypes from 'prop-types';

export const types = {
	addNewCourse: PropTypes.func.isRequired,
	courses: PropTypes.array.isRequired,
	getCourses: PropTypes.func.isRequired,
	searchCourse: PropTypes.func.isRequired,
};
