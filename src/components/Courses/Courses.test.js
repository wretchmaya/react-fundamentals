import React from 'react';
import Courses from './Courses';
import { render } from '@testing-library/react';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import { Router } from 'react-router-dom';
import { createMemoryHistory } from 'history';

const mockStore = configureStore([]);
const store = mockStore({});
const history = createMemoryHistory();

describe('CourseCard component', () => {
	let props;
	beforeEach(() => {
		props = {
			addNewCourse: jest.fn(),
			getCourses: jest.fn(),
			searchCourse: jest.fn(),
			user: {
				token: '123',
			},
			courses: [{ title: 'course1' }, { title: 'course2' }],
		};
	});
	const buildComponent = (props) =>
		render(
			<Provider store={store}>
				<Router history={history}>
					<Courses {...props} />
				</Router>
			</Provider>
		);

	test('should display course cards', () => {
		const { queryAllByTestId } = buildComponent(props);
		expect(queryAllByTestId('course').length).toBe(props.courses.length);
	});

	test('should not display course cards', () => {
		const newProps = {
			courses: [],
		};
		const { queryAllByTestId } = buildComponent(props);
		expect(queryAllByTestId('course').length).toBe(newProps.courses.length);
	});
});
