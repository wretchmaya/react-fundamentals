import Courses from './Courses';
import { connect } from 'react-redux';
import { searchCourseAction } from '../../redux/courses/actionCreators';
import { thunkGetProfileInfo } from '../../redux/user/thunk';
import { thunkGetCoursesRequest } from '../../redux/courses/thunk';
import { thunkGetAuthorsRequest } from '../../redux/authors/thunk';

export const mapStateToProps = (state) => ({
	courses: state.courses.courses,
	user: state.user,
});

export const mapDispatchToProps = (dispatch) => ({
	getCourses: () => dispatch(thunkGetCoursesRequest()),
	searchCourse: (props) => dispatch(searchCourseAction(props)),
	getAuthors: () => dispatch(thunkGetAuthorsRequest()),
	getProfileInfo: (props) => dispatch(thunkGetProfileInfo(props)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Courses);
