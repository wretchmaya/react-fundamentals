import React, { useState, useEffect, useCallback } from 'react';
import Search from '../Search/Search';
import styles from './style.module.scss';
import CourseCard from '../CourseCard/index.js';
import { types } from './types';

const Courses = ({
	addNewCourse,
	courses,
	getAuthors,
	searchCourse,
	getCourses,
	getProfileInfo,
	user,
}) => {
	const [searchQuery, setSearchQuery] = useState('epmty');

	const saveSearchValue = useCallback(
		(e) => {
			setSearchQuery(e.target.value);
		},
		[setSearchQuery]
	);

	useEffect(() => {
		getAuthors();
		getCourses();
	}, [getCourses, getAuthors]);

	useEffect(() => {
		getProfileInfo(user.token);
	}, [user.token, getProfileInfo]);

	useEffect(() => {
		if (!searchQuery) {
			getCourses();
			setSearchQuery('epmty');
		}
	}, [searchQuery, getCourses]);

	const performSearch = (e) => {
		e.preventDefault();
		if (searchQuery !== 'epmty') {
			const searchQueryRegExp = new RegExp(
				searchQuery.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&'),
				'i'
			);
			const searchResult = courses.filter((course) => {
				const titleResult = searchQueryRegExp.test(course.title);
				const idResult = searchQueryRegExp.test(course.id);
				return titleResult || idResult;
			});
			searchCourse(searchResult);
		}
		return;
	};

	return (
		<div className={styles['content-wrapper']} data-testid='courses'>
			<Search
				addNewCourse={addNewCourse}
				saveSearchValue={saveSearchValue}
				performSearch={performSearch}
				role={user.role}
			/>
			{courses.map((course, i) => {
				return (
					<div data-testid='course' key={course.id}>
						<CourseCard
							id={course.id}
							title={course.title}
							description={course.description}
							creationDate={course.creationDate}
							duration={course.duration}
							authorsId={course.authors}
							key={i}
							user={user}
						/>
					</div>
				);
			})}
		</div>
	);
};

Courses.propTypes = types;

export default Courses;
