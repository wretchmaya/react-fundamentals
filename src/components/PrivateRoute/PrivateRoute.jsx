import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { types } from './types';

const PrivateRoute = ({ isAllowed, redirectTo, children, ...rest }) => {
	return (
		<Route
			{...rest}
			render={({ location }) =>
				isAllowed ? (
					children
				) : (
					<Redirect
						to={{
							pathname: redirectTo,
							state: { from: location },
						}}
					/>
				)
			}
		/>
	);
};

PrivateRoute.propTypes = types;

export default PrivateRoute;
