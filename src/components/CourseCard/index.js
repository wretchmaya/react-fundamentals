import CourseCard from './CourseCard';
import { connect } from 'react-redux';
import { thunkDeleteCourseByIdRequest } from '../../redux/courses/thunk';
import { withRouter } from 'react-router';
import {
	getCurrentCourseForUpdate,
	resetFormStateAction,
} from '../../redux/courses/actionCreators';

export const mapDispatchToProps = (dispatch) => ({
	deleteCourse: (props) => dispatch(thunkDeleteCourseByIdRequest(props)),
	getCurrentCourse: (props) => dispatch(getCurrentCourseForUpdate(props)),
	resetForm: () => dispatch(resetFormStateAction()),
});

export default withRouter(connect(null, mapDispatchToProps)(CourseCard));
