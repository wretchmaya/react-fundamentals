import React from 'react';
import CourseCard from './CourseCard';
import { render } from '@testing-library/react';
import { minutesToHours } from '../../helpers/minutesToHours';

describe('CourseCard component', () => {
	let props;
	beforeEach(() => {
		props = {
			id: '1234',
			title: 'Title',
			description: 'Description',
			duration: 123,
			authorsId: ['123'],
			creationDate: '12/12/12',
			user: {
				token: '123',
				role: 'test',
			},
		};
	});
	const buildComponent = (props) => render(<CourseCard {...props} />);

	test('should have title', () => {
		const { getByTestId } = buildComponent(props);
		expect(getByTestId('courseCard__title')).toBeTruthy();
	});

	test('should have description', () => {
		const { getByTestId } = buildComponent(props);
		expect(getByTestId('courseCard__description')).toBeTruthy();
	});

	test('should have piped duration', () => {
		const { getByTestId } = buildComponent(props);
		const pipedDuration = minutesToHours(props.duration);
		expect(getByTestId('courseCard__duration').textContent).toBe(pipedDuration);
	});

	test('should have list of authors', () => {
		const { getByTestId } = buildComponent(props);
		expect(getByTestId('courseCard__authors').children.length).toBe(
			props.authorsId.length
		);
	});

	test('should display creation date', () => {
		const { getByTestId } = buildComponent(props);
		expect(getByTestId('courseCard__creation-date').textContent).toBe(
			props.creationDate
		);
	});
});
