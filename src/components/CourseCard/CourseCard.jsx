import React, { useEffect, useState } from 'react';
import styles from './style.module.scss';
import Button from '../Button/Button';
import { minutesToHours } from '../../helpers/minutesToHours';
import { types } from './types';
import { getAuthorByIdRequest } from '../../api/authors/authors';

const CourseCard = ({
	id,
	title,
	description,
	creationDate,
	duration,
	authorsId,
	deleteCourse,
	user,
	getCurrentCourse,
	resetForm,
	...props
}) => {
	const [decodedAuthors, setDecodedAuthors] = useState([]);
	const isAdmin = user.role === 'admin';

	useEffect(() => {
		let isCancelled = false;
		authorsId.forEach(async (authorId) => {
			const res = await getAuthorByIdRequest(authorId);
			if (!isCancelled) {
				setDecodedAuthors((prevState) => [...prevState, res.data.result.name]);
			}
		});
		return () => (isCancelled = true);
	}, []); // authorsId

	const showMore = () => {
		props.history.push(`/courses/${id}`);
	};

	const removeCourse = () => {
		deleteCourse({ id, token: user.token });
	};

	const updateCourse = () => {
		resetForm();
		getCurrentCourse(id);
		props.history.push(`/courses/update/${id}`);
	};

	return (
		<div className={styles.courseCard}>
			<div className={styles.courseCard__left}>
				<h2 data-testid='courseCard__title'>{title}</h2>
				<p data-testid='courseCard__description'>{description}</p>
			</div>

			<div className={styles.courseCard__right}>
				<p data-testid='courseCard__authors'>
					<span className={styles['courseCard__info-bold']}>Authors: </span>
					{decodedAuthors.map((author, i) => {
						return <span key={i}>{[i > 0 && ', ', author]}</span>;
					})}
				</p>
				<p>
					<span className={styles['courseCard__info-bold']}>Duration: </span>
					<span data-testid='courseCard__duration'>
						{minutesToHours(duration)}
					</span>{' '}
					hours
				</p>
				<p>
					<span className={styles['courseCard__info-bold']}>Created: </span>
					<span data-testid='courseCard__creation-date'>{creationDate}</span>
				</p>
				<div className={styles['courseCard__edit_buttons']}>
					<Button text={'Show course'} onClick={showMore} />
					{isAdmin && (
						<>
							<Button
								text={'Edit'}
								customClass={'edit'}
								onClick={updateCourse}
							/>
							<Button
								text={'Del...'}
								onClick={removeCourse}
								customClass={'edit'}
							/>
						</>
					)}
				</div>
			</div>
		</div>
	);
};

CourseCard.propTypes = types;

export default CourseCard;
