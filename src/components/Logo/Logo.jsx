import React from 'react';

const Logo = () => {
	return <h2 className='logo'>React Fundamentals #5</h2>;
};

export default Logo;
