import React from 'react';
import Button from '../Button/Button';
import Input from '../Input/Input';
import styles from './style.module.scss';
import { types } from './types';

const Search = ({ addNewCourse, saveSearchValue, performSearch, role }) => {
	const isAdmin = role === 'admin';
	return (
		<div className={styles.search}>
			<form className={styles.wrapper} onSubmit={performSearch}>
				<Input onChange={saveSearchValue} placeholder={'Search...'} />
				<Button text={'Search'} />
			</form>
			{isAdmin && (
				<Button
					text={'Add new course'}
					onClick={addNewCourse}
					customClass={'add_button'}
				/>
			)}
		</div>
	);
};

Search.propTypes = types;

export default Search;
