import PropTypes from 'prop-types';

export const types = {
	addNewCourse: PropTypes.func.isRequired,
	saveSearchValue: PropTypes.func.isRequired,
	performSearch: PropTypes.func.isRequired,
};
