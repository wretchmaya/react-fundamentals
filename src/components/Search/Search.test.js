import React from 'react';
import Search from './Search';
import { render, screen, fireEvent } from '@testing-library/react';

describe('CourseCard component', () => {
	let props;
	beforeEach(() => {
		props = {
			addNewCourse: jest.fn(),
			saveSearchValue: jest.fn(),
			performSearch: jest.fn(),
			role: 'admin',
		};
	});
	const buildComponent = (props) => render(<Search {...props} />);

	test('should open course form after click on "add new course"', () => {
		const { getByText } = buildComponent(props);
		const button = getByText('Add new course');
		fireEvent.click(button);
		screen.debug();
		expect(props.addNewCourse).toHaveBeenCalled();
	});
});
