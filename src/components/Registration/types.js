import PropTypes from 'prop-types';

export const types = {
	registerUser: PropTypes.func.isRequired,
	isRegisteredSuccessfully: PropTypes.bool.isRequired,
	resetState: PropTypes.func.isRequired,
};
