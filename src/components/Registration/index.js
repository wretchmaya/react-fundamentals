import Registration from './Registration';
import { connect } from 'react-redux';
import { thunkRegisterRequest } from '../../redux/user/thunk';
import { withRouter } from 'react-router';
import { resetStateAction } from '../../redux/user/actionCreators';

export const mapStateToProps = (state) => {
	return {
		isRegisteredSuccessfully: state.user.isRegisteredSuccessfully,
	};
};

export const mapDispatchToProps = (dispatch) => {
	return {
		registerUser: (props) => dispatch(thunkRegisterRequest(props)),
		resetState: () => dispatch(resetStateAction()),
	};
};

export default withRouter(
	connect(mapStateToProps, mapDispatchToProps)(Registration)
);
