import React, { useEffect, useState } from 'react';
import styles from './style.module.scss';
import Input from '../Input/Input';
import Button from '../Button/Button';
import { Link } from 'react-router-dom';
import { withRouter } from 'react-router';
import { types } from './types';

const Registration = ({
	registerUser,
	isRegisteredSuccessfully,
	resetState,
	...props
}) => {
	const [userCredentials, setUserCredentials] = useState({
		name: '',
		email: '',
		password: '',
	});
	const [isUserDataValid, setIsUserDataValid] = useState(false);

	const onSubmit = (e) => {
		e.preventDefault();
		if (
			userCredentials.name &&
			userCredentials.email &&
			userCredentials.password
		) {
			setIsUserDataValid(true);
		}
	};

	useEffect(() => {
		if (isUserDataValid) {
			registerUser(userCredentials);
		}
		setIsUserDataValid(false);
	}, [isUserDataValid, userCredentials, registerUser]);

	useEffect(() => {
		if (isRegisteredSuccessfully) {
			alert('You have been successfully registered');
			props.history.push('/login');
		}
	}, [isRegisteredSuccessfully, props.history]);

	useEffect(() => {
		return () => resetState();
	}, [resetState]);

	return (
		<div className={styles.registration}>
			<h2 className={styles.registration__title}>Registration</h2>
			<form onSubmit={onSubmit} className={styles.registration__form}>
				<label>
					Name
					<Input
						type={'text'}
						placeholder={'Enter name'}
						onChange={(e) =>
							setUserCredentials({
								...userCredentials,
								name: e.target.value,
							})
						}
					/>
				</label>
				<label>
					Email
					<Input
						type={'email'}
						placeholder={'Enter email'}
						onChange={(e) =>
							setUserCredentials({
								...userCredentials,
								email: e.target.value,
							})
						}
					/>
				</label>
				<label>
					Password
					<Input
						type={'password'}
						placeholder={'Enter password'}
						onChange={(e) =>
							setUserCredentials({
								...userCredentials,
								password: e.target.value,
							})
						}
					/>
				</label>
				<Button text={'Registration'} customClass={'login'} />
			</form>
			<p className={styles.registration__link}>
				If you have an account, please <Link to='/login'>Login</Link>
			</p>
		</div>
	);
};

Registration.propTypes = types;

export default withRouter(Registration);
