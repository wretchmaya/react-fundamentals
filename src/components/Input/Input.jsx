import React from 'react';
import styles from './style.module.scss';
import classNames from 'classnames/bind';
import { types } from './types';

const cx = classNames.bind(styles);

const Input = ({ type, placeholder, value, onChange, customClass }) => {
	return (
		<input
			type={type}
			placeholder={placeholder}
			className={cx('input', customClass)}
			onChange={onChange}
			value={value}
		></input>
	);
};

Input.propTypes = types;

export default Input;
