import PropTypes from 'prop-types';

export const types = {
	type: PropTypes.string,
	placeholder: PropTypes.string.isRequired,
	value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
	onChange: PropTypes.func.isRequired,
	customClass: PropTypes.string,
};
