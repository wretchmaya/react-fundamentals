import React, { useEffect, useState } from 'react';
import styles from './style.module.scss';
import { Link, useParams } from 'react-router-dom';
import { minutesToHours } from '../../helpers/minutesToHours';
import { useFetchData } from '../../hooks/useFetchData';
import { getCourseByIdRequest } from '../../api/courses/courses';

const CourseInfo = () => {
	const { id } = useParams();
	const [course, setCourse] = useState({
		title: '',
		creationDate: '',
		duration: null,
		description: '',
		id: '',
		authors: [],
	});
	const [decodedAuthors, setDecodedAuthors] = useState([]);

	const [{ data: fetchedAuthors }] = useFetchData(
		'http://localhost:3000/authors/all'
	);

	useEffect(() => {
		const getCourse = async () => {
			const response = await getCourseByIdRequest(id);
			setCourse({ ...response.data.result });
		};
		getCourse();
	}, [id]);

	useEffect(() => {
		course.authors.forEach((authorId) => {
			[...fetchedAuthors].forEach((fetchedAuthor) => {
				if (authorId === fetchedAuthor.id) {
					setDecodedAuthors((prevState) => [...prevState, fetchedAuthor.name]);
				}
			});
		});
	}, [course.authors, fetchedAuthors]);

	return (
		<div className={styles.course}>
			<div className={styles.course__link}>
				<Link to={'/courses'}>Back to courses</Link>
			</div>
			<h1 className={styles.course__title}>{course.title}</h1>
			<div className={styles.course__left}>
				<p>{course.description}</p>
			</div>
			<div className={styles.course__right}>
				<p>
					<span className={styles.course__info}>ID: </span>
					{course.id}
				</p>
				<p>
					<span className={styles.course__info}>Duration: </span>
					{minutesToHours(course.duration)} hours
				</p>
				<p>
					<span className={styles.course__info}>Created: </span>
					{course.creationDate}
				</p>
				<div className={styles.course__authors}>
					<span className={styles.course__info}>Authors: </span>
					<ul>
						{decodedAuthors.map((author, i) => {
							return <li key={i}>{author}</li>;
						})}
					</ul>
				</div>
			</div>
		</div>
	);
};

export default CourseInfo;
