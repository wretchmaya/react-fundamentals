import Header from './Header';
import { connect } from 'react-redux';
import { thunkLogOutRequest } from '../../redux/user/thunk';

export const mapStateToProps = (state) => ({
	user: state.user,
});

export const mapDispatchToProps = (dispatch) => ({
	logOutUser: (props) => dispatch(thunkLogOutRequest(props)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Header);
