import PropTypes from 'prop-types';

export const types = {
	user: PropTypes.object.isRequired,
	logOutUser: PropTypes.func.isRequired,
};
