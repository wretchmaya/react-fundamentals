import React from 'react';
import Logo from '../Logo/Logo';
import Button from '../Button/Button';
import styles from './style.module.scss';
import { types } from './types';

const Header = ({ user, logOutUser }) => {
	const logOut = () => {
		logOutUser(user.token);
	};

	return (
		<div className={styles.header}>
			<Logo />
			{user.isAuth && (
				<>
					<h4 className={styles['header__user-name']} data-testid='user-name'>
						{user.name}
					</h4>
					<Button text={'Logout'} onClick={logOut} />{' '}
				</>
			)}
		</div>
	);
};

Header.propTypes = types;
export default Header;
