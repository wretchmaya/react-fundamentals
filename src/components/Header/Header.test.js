import React from 'react';
import Header from './Header';
import { render } from '@testing-library/react';

describe('CourseCard component', () => {
	let props;
	beforeEach(() => {
		props = {
			logOutUser: jest.fn(),
			user: {
				isAuth: true,
			},
		};
	});
	const buildComponent = (props) => render(<Header {...props} />);

	test('should have logo displayed', () => {
		const { container } = buildComponent(props);
		expect(container.querySelector('.logo')).toBeTruthy();
	});

	test('should have user name displayed', () => {
		const { getByTestId } = buildComponent(props);
		expect(getByTestId('user-name')).toBeTruthy();
	});

	test('should have logout button', () => {
		const { getByText } = buildComponent(props);
		expect(getByText('Logout')).toBeTruthy();
	});
});
