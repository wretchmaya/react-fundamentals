import PropTypes from 'prop-types';

export const types = {
	placeholder: PropTypes.string.isRequired,
	onChange: PropTypes.func.isRequired,
};
