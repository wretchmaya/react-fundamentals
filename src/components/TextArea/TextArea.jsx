import React from 'react';
import styles from './style.module.scss';
import { types } from './types';

const TextArea = ({ placeholder, onChange, value }) => {
	return (
		<textarea
			className={styles.textarea}
			placeholder={placeholder}
			minLength={2}
			onChange={onChange}
			value={value}
		></textarea>
	);
};

TextArea.propTypes = types;

export default TextArea;
