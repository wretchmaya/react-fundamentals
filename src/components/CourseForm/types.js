import PropTypes from 'prop-types';
export const types = {
	closeCourseForm: PropTypes.func.isRequired,
	token: PropTypes.string.isRequired,
	addNewAuthor: PropTypes.func.isRequired,
	fetchedAuthors: PropTypes.array.isRequired,
};
