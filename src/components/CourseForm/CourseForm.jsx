import React, { useEffect, useState } from 'react';
import styles from './style.module.scss';
import Button from '../Button/Button';
import Input from '../Input/Input';
import TextArea from '../TextArea/TextArea';
import classNames from 'classnames/bind';
import { v4 as uuidv4 } from 'uuid';
import { minutesToHours } from '../../helpers/minutesToHours';
import { correctDateFormat } from '../../helpers/correctDateFormat';
import { types } from './types';
import { useParams } from 'react-router-dom';

const cx = classNames.bind(styles);

const CourseForm = ({
	closeCourseForm,
	token,
	fetchedAuthors,
	postCourse,
	currentCourse,
	addNewAuthor,
	updateCourseById,
	resetFormState,
}) => {
	const { id } = useParams();

	const [courseData, setCourseData] = useState({
		title: '',
		description: '',
		creationDate: correctDateFormat(),
		duration: '',
		authors: [],
		id: id ? '' : uuidv4(),
	});
	const currentCourseNotEmptyAndExists =
		currentCourse &&
		Object.keys(currentCourse).length &&
		currentCourse.constructor === Object;

	const [authors, setAuthors] = useState([...fetchedAuthors]);

	const [createdAuthor, setCreatedAuthor] = useState({
		name: '',
	});
	const [courseAuthors, setCourseAuthors] = useState([]);
	const [courseDataValid, SetCourseDataValid] = useState(false);

	const createCourse = () => {
		if (isCourseValid(courseData)) {
			SetCourseDataValid(true);
		} else {
			alert('Please fill in all fields');
		}
	};
	useEffect(() => {
		if (currentCourseNotEmptyAndExists) {
			currentCourse.authors.forEach((authorId) => {
				const decoded = fetchedAuthors.find((author) => author.id === authorId);
				setCourseAuthors((prevState) => [...prevState, decoded]);
			});
			setCourseData({ ...currentCourse });
		}
	}, [currentCourse, fetchedAuthors, currentCourseNotEmptyAndExists]);

	useEffect(() => {
		if (currentCourseNotEmptyAndExists) {
			const newAuthors = authors.filter((a) => {
				return (
					courseAuthors.filter((f) => {
						return a.id === f.id;
					}).length === 0
				);
			});
			setAuthors([...newAuthors]);
		}
	}, [courseAuthors, currentCourse, currentCourseNotEmptyAndExists]);

	useEffect(() => {
		if (courseDataValid) {
			currentCourseNotEmptyAndExists
				? updateCourseById({ courseData, token })
				: postCourse({ courseData, token });
			closeCourseForm();
		}
		SetCourseDataValid(false);
	}, [
		courseDataValid,
		courseData,
		closeCourseForm,
		token,
		postCourse,
		updateCourseById,
		currentCourse,
		currentCourseNotEmptyAndExists,
	]);

	useEffect(() => {
		return () => {
			resetFormState();
		};
	}, []);

	const isCourseValid = (course) => {
		return (
			course.title &&
			course.title.length > 2 &&
			course.description &&
			course.description.length > 2 &&
			course.duration &&
			course.authors
		);
	};

	const createAuthor = (e) => {
		e.preventDefault();
		if (createdAuthor.name) {
			addNewAuthor({ name: createdAuthor.name, token });
			setCreatedAuthor({
				name: '',
			});
		}
		return;
	};

	useEffect(() => {
		setAuthors([...fetchedAuthors]);
	}, [fetchedAuthors]);

	const addCourseAuthor = (author) => {
		setCourseAuthors([...courseAuthors, author]);
		setAuthors([...authors.filter((a) => a.id !== author.id)]);
		setCourseData({
			...courseData,
			authors: [...courseData.authors, author.id],
		});
	};

	const deleteCourseAuthor = (author) => {
		const newCourseAuthors = courseAuthors.filter((a) => a.id !== author.id);
		setCourseAuthors([...newCourseAuthors]);
		setAuthors([author, ...authors]);
		setCourseData({
			...courseData,
			authors: [...newCourseAuthors.map((author) => author.id)],
		});
	};

	const createCourseDuration = (e) => {
		setCourseData({ ...courseData, duration: +e.target.value });
	};

	return (
		<div className={cx('create-course')}>
			<div className={cx('title-wrapper')}>
				<label>
					Title
					<Input
						onChange={(e) =>
							setCourseData({ ...courseData, title: e.target.value })
						}
						placeholder={'Enter title...'}
						value={courseData.title}
					/>
				</label>
				<Button
					onClick={createCourse}
					text={
						currentCourseNotEmptyAndExists ? 'Update course' : 'Create course'
					}
				/>
			</div>

			<div className={cx('description-wrapper')}>
				<label className={cx('description')}>
					Description:
					<TextArea
						placeholder={'Enter description'}
						onChange={(e) =>
							setCourseData({ ...courseData, description: e.target.value })
						}
						value={courseData.description}
					/>
				</label>
			</div>

			<div className={cx('authors-wrapper')}>
				<div className={cx('add-author')}>
					<h3 className={cx('add-author__title')}>Add author</h3>
					<form onSubmit={createAuthor}>
						<label>
							Author name
							<Input
								placeholder={'Enter author name...'}
								onChange={(e) =>
									setCreatedAuthor({
										name: e.target.value,
									})
								}
								value={createdAuthor.name}
							/>
						</label>
						<Button text={'Create author'} />
					</form>
				</div>

				<div className={cx('authors')}>
					<h3 className={cx('authors__title')}>Authors</h3>
					{authors.map((author, i) => {
						return (
							<div
								key={i}
								className={cx('authors__author')}
								data-testid='author'
							>
								<p>{author.name}</p>
								<Button
									text={'Add author'}
									onClick={() => addCourseAuthor(author)}
								/>
							</div>
						);
					})}
				</div>

				<div className={cx('duration')}>
					<h3 className={cx('duration__title')}>Duration</h3>
					<label>
						Duration
						<Input
							placeholder={'Enter duration in minutes...'}
							onChange={(e) => createCourseDuration(e)}
							type={'number'}
							value={courseData.duration}
						/>
					</label>
					<p className={cx('duration__time')}>
						Duration:{' '}
						{courseData.duration ? (
							<span className={cx('duration__value')}>
								{minutesToHours(courseData.duration)}
							</span>
						) : (
							<span className={cx('duration__value')}>00:00</span>
						)}{' '}
						hours
					</p>
				</div>

				<div className={cx('course-authors')}>
					<h3 className={cx('course-authors__title')}>Course authors</h3>
					{!courseAuthors.length ? (
						<p
							className={cx('course-authors__author--empty')}
							data-testid='empty-list'
						>
							Author list is empty
						</p>
					) : (
						courseAuthors.map((author, i) => {
							return (
								<div
									key={author.id}
									className={cx('course-authors__author')}
									data-testid='course-author'
								>
									<p>{author.name}</p>
									<Button
										text={'Delete author'}
										onClick={() => deleteCourseAuthor(author)}
									/>
								</div>
							);
						})
					)}
				</div>
			</div>
		</div>
	);
};

CourseForm.propTypes = types;

export default CourseForm;
