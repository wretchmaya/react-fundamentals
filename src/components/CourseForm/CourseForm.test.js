import React from 'react';
import CourseForm from './CourseForm';
import { render, fireEvent, screen, getByText } from '@testing-library/react';
import { Router } from 'react-router-dom';
import { createMemoryHistory } from 'history';
const history = createMemoryHistory();

describe('CourseCard component', () => {
	let props;
	beforeEach(() => {
		props = {
			closeCourseForm: jest.fn(),
			token: '123',
			fetchedAuthors: ['123', '123'],
			postCourse: jest.fn(),
			currentCourse: {},
			addNewAuthor: jest.fn(),
			updateCourseById: jest.fn(),
			resetFormState: jest.fn(),
		};
	});

	const buildComponent = (props) =>
		render(
			<Router history={history}>
				<CourseForm {...props} />
			</Router>
		);

	test('should show list of authors', () => {
		const { getAllByTestId } = buildComponent(props);
		expect(getAllByTestId('author').length).toBe(props.fetchedAuthors.length);
	});

	test('should add a created author to the list of all authors', () => {
		const { getByPlaceholderText, getByText } = buildComponent(props);
		const input = getByPlaceholderText('Enter author name...');
		const button = getByText('Create author');
		fireEvent.input(input, { target: { value: 'test' } });
		fireEvent.click(button);
		expect(props.addNewAuthor).toHaveBeenCalled();
	});

	test('should add an author to course authors list', () => {
		const { getAllByText, getByTestId } = buildComponent(props);
		const addAuthor = getAllByText('Add author')[1];
		fireEvent.click(addAuthor);
		const courseAuthor = getByTestId('course-author');
		expect(courseAuthor).toBeTruthy();
	});

	test('should delete an author from course authors list', () => {
		const { getAllByText, getByText, getByTestId } = buildComponent(props);
		const addAuthor = getAllByText('Add author')[1];
		const emptyList = getByTestId('empty-list');
		fireEvent.click(addAuthor);
		const deleteCourseAuthor = getByText('Delete author');
		fireEvent.click(deleteCourseAuthor);
		expect(emptyList).toBeTruthy();
	});
});
