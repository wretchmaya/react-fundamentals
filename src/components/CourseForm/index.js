import CourseForm from './CourseForm';
import { connect } from 'react-redux';
import {
	thunkPostCourseRequest,
	thunkUpdateCourseByIdRequest,
} from '../../redux/courses/thunk';
import { thunkPostNewAuthor } from '../../redux/authors/thunk';
import { resetFormStateAction } from '../../redux/courses/actionCreators';

export const mapStateToProps = (state) => ({
	token: state.user.token,
	fetchedAuthors: state.authors.authors,
	currentCourse: state.courses.currentCourse,
});

export const mapDispatchToProps = (dispatch) => ({
	addNewAuthor: (props) => dispatch(thunkPostNewAuthor(props)),
	postCourse: (props) => dispatch(thunkPostCourseRequest(props)),
	updateCourseById: (props) => dispatch(thunkUpdateCourseByIdRequest(props)),
	resetFormState: () => dispatch(resetFormStateAction()),
});

export default connect(mapStateToProps, mapDispatchToProps)(CourseForm);
