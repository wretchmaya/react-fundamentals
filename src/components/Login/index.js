import Login from './Login';
import { connect } from 'react-redux';
import { thunkLogInRequest } from '../../redux/user/thunk';
import { withRouter } from 'react-router';

export const mapStateToProps = (state) => ({
	user: state.user,
});

export const mapDispatchToProps = (dispatch) => {
	return {
		logInUser: (props) => dispatch(thunkLogInRequest(props)),
	};
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Login));
