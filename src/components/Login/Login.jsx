import React, { useState, useEffect } from 'react';
import styles from './style.module.scss';
import Input from '../Input/Input';
import Button from '../Button/Button';
import { Link } from 'react-router-dom';
import { types } from './types';

const Login = ({ logInUser, user, ...props }) => {
	const [userCredentials, setUserCredentials] = useState({
		email: '',
		password: '',
	});
	const [isUserDataValid, setIsUserDataValid] = useState(false);

	const onSubmit = (e) => {
		e.preventDefault();
		if (userCredentials.email && userCredentials.password) {
			setIsUserDataValid(true);
		}
	};

	useEffect(() => {
		if (isUserDataValid) {
			logInUser(userCredentials);
		}
		setIsUserDataValid(false);
	}, [isUserDataValid, userCredentials, logInUser]);

	useEffect(() => {
		if (user.isAuth) {
			props.history.push('/courses');
		}
	}, [user, props.history]);

	return (
		<div className={styles.login}>
			<h2 className={styles.login__title}>Login</h2>
			<form onSubmit={onSubmit} className={styles.login__form}>
				<label>
					Email
					<Input
						type={'email'}
						placeholder={'Enter email'}
						onChange={(e) =>
							setUserCredentials({
								...userCredentials,
								email: e.target.value,
							})
						}
					/>
				</label>
				<label>
					Password
					<Input
						type={'password'}
						placeholder={'Enter password'}
						onChange={(e) =>
							setUserCredentials({
								...userCredentials,
								password: e.target.value,
							})
						}
					/>
				</label>
				<Button text={'Login'} customClass={'login'} />
			</form>
			<p className={styles.login__link}>
				If you don't have an account, please{' '}
				<Link to='/registration'>Register</Link> one
			</p>
		</div>
	);
};

Login.propTypes = types;

export default Login;
