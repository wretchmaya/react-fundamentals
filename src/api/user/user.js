import axios from 'axios';

export const logInRequest = (userCredentials) => {
	return axios
		.post('http://localhost:3000/login', userCredentials)
		.catch((error) => {
			throw error;
		});
};

export const registerRequest = (userCredentials) => {
	return axios
		.post('http://localhost:3000/register', userCredentials)
		.catch((error) => {
			throw error;
		});
};

export const logOutRequest = (token) => {
	return axios
		.delete('http://localhost:3000/logout', {
			headers: { Authorization: token },
		})
		.catch((error) => {
			throw error;
		});
};

export const getProfileInfoRequest = (token) => {
	return axios
		.get('http://localhost:3000/users/me', {
			headers: { Authorization: token },
		})
		.catch((error) => {
			throw error;
		});
};
