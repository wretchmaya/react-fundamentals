import axios from 'axios';

export const getCourseByIdRequest = (id) => {
	return axios.get(`http://localhost:3000/courses/${id}`).catch((error) => {
		throw error;
	});
};

export const postCourseRequest = (courseData, token) => {
	return axios
		.post('http://localhost:3000/courses/add', courseData, {
			headers: { Authorization: token },
		})
		.catch((error) => {
			throw error;
		});
};

export const deleteCourseByIdRequest = (id, token) => {
	return axios
		.delete(`http://localhost:3000/courses/${id}`, {
			headers: { Authorization: token },
		})
		.catch((error) => {
			throw error;
		});
};

export const getCoursesRequest = () => {
	return axios.get('http://localhost:3000/courses/all').catch((error) => {
		throw error;
	});
};

export const updateCourseByIdRequest = (courseData, courseId, token) => {
	return axios
		.put(`http://localhost:3000/courses/${courseId}`, courseData, {
			headers: { Authorization: token },
		})
		.catch((error) => {
			throw error;
		});
};
