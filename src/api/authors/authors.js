import axios from 'axios';

export const getAuthorsRequest = () => {
	return axios.get('http://localhost:3000/authors/all').catch((error) => {
		throw error;
	});
};

export const addNewAuthorRequest = (author, token) => {
	return axios
		.post('http://localhost:3000/authors/add', author, {
			headers: { Authorization: token },
		})
		.catch((error) => {
			throw error;
		});
};

export const getAuthorByIdRequest = (id) => {
	return axios.get(`http://localhost:3000/authors/${id}`).catch((error) => {
		throw error;
	});
};
