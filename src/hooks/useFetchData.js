import { useState, useEffect } from 'react';
import axios from 'axios';

export const useFetchData = (initialUrl) => {
	const [data, setData] = useState('');
	const [url, setUrl] = useState(initialUrl);

	useEffect(() => {
		const fetchData = async () => {
			const result = await axios.get(`${url}`);
			setData(result.data.result);
		};
		fetchData();
	}, [url]);
	return [{ data }, setUrl];
};
